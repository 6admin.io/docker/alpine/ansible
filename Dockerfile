# Dockerfile for building ansible toolkit
FROM alpine:3.17.10 AS builder

ARG VERSION=2.17.0

ENV PIP_CONSTRAINT="/tmp/constraint.txt"

COPY constraint.txt /tmp/constraint.txt

RUN set -xe && \
        apk update && \
        apk add --no-cache --purge --virtual .build-deps -Uu gcc musl-dev libffi-dev libxslt-dev yaml-dev python3 python3-dev py3-pip && \
        python3 -m venv /opt/venv && \
        source /opt/venv/bin/activate && \
        python3 -m pip install --no-cache-dir --upgrade pip setuptools && \
        python3 -m pip install --no-cache-dir --no-compile PyMySQL docker docker-compose pyvmomi passlib google_auth netaddr lxml paramiko ovh ansible-core==$VERSION && \
        python3 -m pip uninstall --yes setuptools wheel && \
        apk del .build-deps

FROM alpine:3.17.10 AS default

ARG VERSION=2.17.0

LABEL "maintainer"="G. K. E. <gke@6admin.io>"
LABEL "org.opencontainers.image.authors"="G. K. E. <gke@6admin.io>"
LABEL "org.opencontainers.image.vendor"="6admin"
LABEL "org.opencontainers.image.licenses"="GPL3"
LABEL "org.opencontainers.image.url"="https://gitlab.com/6admin/docker/alpine/ansible"
LABEL "org.opencontainers.image.documentation"="https://gitlab.com/6admin/docker/alpine/ansible"
LABEL "org.opencontainers.image.source"="https://gitlab.com/6admin/docker/alpine/ansible"
LABEL "org.opencontainers.image.ref.name"="Ansible ${VERSION}"
LABEL "org.opencontainers.image.title"="Ansible ${VERSION}"
LABEL "org.opencontainers.image.description"="Ansible ${VERSION}"

COPY --from=builder /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"

RUN set xe && \
        apk add --no-cache --purge python3 py3-yaml py3-cryptography git sudo curl ca-certificates openssh-client sshpass rsync && \
        ansible-galaxy collection install community.general && \
        ansible-galaxy collection install community.vmware && \
        ansible-galaxy collection install community.aws && \
        ansible-galaxy collection install community.kubernetes && \
        ansible-galaxy collection install community.libvirt && \
        ansible-galaxy collection install community.docker && \
        ansible-galaxy collection install google.cloud && \
        ansible-galaxy collection install git+https://github.com/synthesio/infra-ovh-ansible-module && \
        find /usr/lib/ -type d -name '__pycache__' -delete && \
        find /usr/lib/ -type f -name '*.pyc' -delete && \
        find /opt/venv/lib/ -type d -name '__pycache__' -delete && \
        find /opt/venv/lib/ -type f -name '*.pyc' -delete && \
        rm -rf /tmp/* /var/tmp/* 

WORKDIR /src

ENTRYPOINT ["ansible"]
CMD ["--version"]
